
//判斷裝置
var userAgent = navigator.userAgent || navigator.vendor || window.opera;
var isAndroid = (/android/i.test(userAgent)) ? true : false;
var isiOS = (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) ? true : false;
var device
if(isAndroid){
  device = 'adr'
}else if(isiOS){
  device = 'ios'
}else {
  device = 'pc'
}
console.log(location.href)
let siteUrl = location.href
let siteSource = siteUrl.slice(siteUrl.indexOf('=')+1,siteUrl.indexOf('&'))
let siteMedium = 'medium='
siteMedium = siteUrl.slice(siteUrl.indexOf(siteMedium)+siteMedium.length)
if(siteMedium.indexOf('&')!=-1){
  siteMedium = siteMedium.slice(0,siteUrl.indexOf('&'))
}
trackEvent(siteSource,siteMedium,' click')

//點擊複製功能
function copyShareWords(id, pop_id) {
  var copyText = document.getElementById(id);
  copyText.style.display = "block";
  copyText.select();
  copyText.setSelectionRange(0, 512);
  document.execCommand("copy");
  window.getSelection().removeAllRanges();
  copyText.style.display = "none";
  toastAppear(pop_id);
}
//吐司方法
var toastAppear = (toastID) => {
  console.log(toastID)
  document.getElementById(toastID).style.display = "block";
  setTimeout(function () {
    document.getElementById(toastID).style.display = "none";
  }, 3000);
}

var bImgHost = 'https://b.gtgfj.net/';

function getBImg(url, id) {
  console.log('url:' + url)
  var urls = [];
  var url64 = '';
  var img64Prefix = '';
  if (url.indexOf('.jpg') != -1) {
    urls = url.split('.jpg');
    url64 = urls[0];
    img64Prefix = 'data:image/jpeg;base64,'
  } else if (url.indexOf('.jpeg') != -1) {
    urls = url.split('.jpeg');
    url64 = urls[0] + '/j';
    img64Prefix = 'data:image/jpeg;base64,'
  } else if (url.indexOf('.png') != -1) {
    urls = url.split('.png');
    url64 = urls[0] + '/p';
    img64Prefix = 'data:image/png;base64,'
  } else if (url.indexOf('.gif') != -1) {
    urls = url.split('.gif');
    url64 = urls[0] + '/g';
    img64Prefix = 'data:image/gif;base64,'
  }
  $.ajax({
    url: url64,
    success: function (data) {
      console.log(url64)
      console.log(data)
      $('#' + id).attr('src', img64Prefix + data.substr(1));
    },
  });
}
function getBImgClass(url, className, index) {
  var urls = [];
  var url64 = '';
  var img64Prefix = '';
  if (url.indexOf('.jpg') != -1) {
    urls = url.split('.jpg');
    url64 = urls[0];
    img64Prefix = 'data:image/jpeg;base64,'
  } else if (url.indexOf('.jpeg') != -1) {
    urls = url.split('.jpeg');
    url64 = urls[0] + '/j';
    img64Prefix = 'data:image/jpeg;base64,'
  } else if (url.indexOf('.png') != -1) {
    urls = url.split('.png');
    url64 = urls[0] + '/p';
    img64Prefix = 'data:image/png;base64,'
  } else if (url.indexOf('.gif') != -1) {
    urls = url.split('.gif');
    url64 = urls[0] + '/g';
    img64Prefix = 'data:image/gif;base64,'
  }
  $.ajax({
    url: url64,
    success: function (data) {
      $('.' + className).attr('src', img64Prefix + data.substr(1));
    },
  });
}
/// 排序功能

function rearrangeOrder(arr){
  arr.sort(function(a, b) {
    return a.sortNum - b.sortNum;
  });
}

///呼叫json

$.get('./js/souying.json', function (response,status,xhr) {
  console.log(response)
  //複製按鈕資料
  $('#copyUrl').val(response.data.board.copy);
  $('.jsCopy').text(response.data.board.content);
  //banner資料帶入
  getBImgClass(bImgHost + response.data.banner.image, 'jsBannerImg');
  $('.jsBannerUrl').attr('href',response.data.banner[device])
  $('.jsBannerUrl').click(function() {
    trackEvent('首頁-BANNER',response.data.banner.title,response.data.banner[device])
  });
  //帶入熱搜榜資料
  for(let i=0;i<3;i++){
    getBImgClass(bImgHost + response.data.hotsearch.content[i].image, 'jsHotSearchImg'+i);
    $('.jsHotSearchTitle'+i).text(response.data.hotsearch.content[i].title);
    $('.jsHotSearchText'+i).text(response.data.hotsearch.content[i].intro);
    $('.jsHotSearchUrl'+i).attr('href',response.data.hotsearch.content[i][device])
    $('.jsHotSearchUrl'+i).click(function() {
      trackEvent('首頁-热搜榜',response.data.hotsearch.content[i].title+'-'+i,response.data.hotsearch.content[i][device])
    });
  }
  let hotsearchStr = ''
  for(let i=3;i<response.data.hotsearch.content.length;i++){
    hotsearchStr += `<a  href="${response.data.hotsearch.content[i][device]}" target="_blank" onclick="trackEvent('首頁-热搜榜', '${response.data.hotsearch.content[i].title}-${i}', '${response.data.hotsearch.content[i][device]}')">`
    hotsearchStr += '<div class="APPWrap">'
    hotsearchStr += '<img class="jsHotSearchImg'+i+'" src="">'
    hotsearchStr += '<p>'+response.data.hotsearch.content[i].title+'</p>'
    hotsearchStr += '</div>'
    hotsearchStr += '</a>'
    getBImgClass(bImgHost + response.data.hotsearch.content[i].image, 'jsHotSearchImg'+i);
  }
  $('.jsHotSearchAPPWrap').append(hotsearchStr)
    /////熱搜結束////
  //帶入精選資料
  for(let i=0;i<10;i++){
    if(i==0){
      $('.jsFeaturedText'+i).text(response.data.featured.content[i].intro);
    }
    getBImgClass(bImgHost + response.data.featured.content[i].image, 'jsFeaturedImg'+i);
    $('.jsFeaturedTitle'+i).text(response.data.featured.content[i].title);
    $('.jsFeaturedUrl'+i).attr('href',response.data.featured.content[i][device])
    $('.jsFeaturedUrl'+i).click(function() {
      trackEvent('首頁-淫鸡精选',response.data.featured.content[i].title+'-'+i,response.data.featured.content[i][device])
    });
  }
  let choiceAPPWrapStr = ''

  for(let i=10;i<response.data.featured.content.length;i++){
    if((i-10)%7==0) {
      choiceAPPWrapStr += '<div class="mainAPPWrap">'
      choiceAPPWrapStr += '<img class="APPImg jsFeaturedImg'+i+'" src="">'
      choiceAPPWrapStr += '<div>'
      choiceAPPWrapStr += '<p class="APPTitle">'+response.data.featured.content[i].title+'</p>'
      choiceAPPWrapStr += '<p class="APPText">'+response.data.featured.content[i].intro+'</p>'
      choiceAPPWrapStr += '</div>'
      choiceAPPWrapStr += `<a class=" downloadWrap"  href="${response.data.featured.content[i][device]}" target="_blank" onclick="trackEvent('首頁-淫鸡精选', '${response.data.featured.content[i].title}-${i}', '${response.data.featured.content[i][device]}')">`
      choiceAPPWrapStr += '<div style="display: flex;">'
      choiceAPPWrapStr += '<img src="./images/down.png">'
      choiceAPPWrapStr += '<p>立即下载</p>'
      choiceAPPWrapStr += '</div>'
      choiceAPPWrapStr += '</a>'
      choiceAPPWrapStr += '</div>'
    }else{
      choiceAPPWrapStr += `<a href="${response.data.featured.content[i][device]}" target="_blank" onclick="trackEvent('首頁-淫鸡精选', '${response.data.featured.content[i].title}-${i}', '${response.data.featured.content[i][device]}')">`
      choiceAPPWrapStr += '<div class="APPWrap">'
      choiceAPPWrapStr += '<img class="jsFeaturedImg'+i+'" src="">'
      choiceAPPWrapStr += '<p>'+response.data.featured.content[i].title+'</p>'
      choiceAPPWrapStr += '</div></a>'
      if((i-10)%7==6) {
        choiceAPPWrapStr += '<div class="clearFloat"></div>'
      }
    }
    $('.jsChoiceAPPWrap').append(choiceAPPWrapStr)
    choiceAPPWrapStr = ''
    getBImgClass(bImgHost + response.data.featured.content[i].image, 'jsFeaturedImg'+i);
  }
  let jsHotUseWrapStr = ''
  for(let i=0;i<response.data.hotapp.content.length;i++){
    jsHotUseWrapStr += `<a href="${response.data.hotapp.content[i][device]}" target="_blank" onclick="trackEvent('首頁-热门应用', '${response.data.hotapp.content[i].title}-${i}', '${response.data.hotapp.content[i][device]}')">`
    jsHotUseWrapStr += '<div class="APPWrap">'
    jsHotUseWrapStr += '<img class="jsHotUseImg'+i+'">'
    jsHotUseWrapStr += '<div class="textWrap">'
    jsHotUseWrapStr += '<p class="APPTitle">'+response.data.hotapp.content[i].title+'</p>'
    jsHotUseWrapStr += '<p class="APPTexy">'+response.data.hotapp.content[i].intro+'</p>'
    jsHotUseWrapStr += '</div>'
    jsHotUseWrapStr += '<div class="downloadBtn">取得</div>'
    jsHotUseWrapStr += '</div>'
    jsHotUseWrapStr += '</a>'
    jsHotUseWrapStr += '<div class="grayLine"></div>'
    $('.jsHotUseWrap').append(jsHotUseWrapStr)
    jsHotUseWrapStr = ''
    getBImgClass(bImgHost + response.data.hotapp.content[i].image, 'jsHotUseImg'+i);
  }
  $('.jsShowAll').attr('href',response.data.hotapp.showall.link)
  $('.jsShowAll').text(response.data.hotapp.showall.text)

  let jsGtagWrapStr = ''
  for(let i=0;i<response.data.bookmark.content.length;i++){
    jsGtagWrapStr += `<a href="${response.data.bookmark.content[i][device]}" target="_blank" onclick="trackEvent('首頁-淫鸡书签', '${response.data.bookmark.content[i].title}-${i}', '${response.data.bookmark.content[i][device]}')">`
    jsGtagWrapStr += '<div class="APPWrap">'
    jsGtagWrapStr += '<img class="jsGtagImg'+i+'">'
    jsGtagWrapStr += '<div>'
    jsGtagWrapStr += '<p>'+response.data.bookmark.content[i].title+'</p>'
    jsGtagWrapStr += '</div>'
    jsGtagWrapStr += '</div>'
    jsGtagWrapStr += '</a>'
    if(response.data.bookmark.content.length-1==i){
      jsGtagWrapStr += '<div class="clearFloat"></div>'
    }
    $('.jsGtagWrap').append(jsGtagWrapStr)
    jsGtagWrapStr = ''
    getBImgClass(bImgHost + response.data.bookmark.content[i].image, 'jsGtagImg'+i);
  }
  let jsBestSite0Str = ''
  for(let i=0;i<response.data.famous.content.length;i++){
    jsBestSite0Str += `<a href="${response.data.famous.content[i][device]}" target="_blank" onclick="trackEvent('首頁-淫鸡书签', '${response.data.famous.content[i].title}-${i}', '${response.data.famous.content[i][device]}')">`
    jsBestSite0Str += '<div class="siteWrap">'
    jsBestSite0Str += '<p>'+response.data.famous.content[i].title+'</p>'
    jsBestSite0Str += '</div>'
    jsBestSite0Str += '</a>'
    if(response.data.famous.content.length-1==i){
      jsBestSite0Str += '<div class="clearFloat"></div>'
    }
  }
  $('.jsBestSite0').append(jsBestSite0Str)
  let jsBestSite1Str = ''
  for(let i=0;i<response.data.beautyg.content.length;i++){
    jsBestSite1Str += `<a href="${response.data.beautyg.content[i][device]}" target="_blank" onclick="trackEvent('首頁-姬太美视频', '${response.data.beautyg.content[i].title}-${i}', '${response.data.beautyg.content[i][device]}')">`
    jsBestSite1Str += '<div class="siteWrap">'
    jsBestSite1Str += '<p>'+response.data.beautyg.content[i].title+'</p>'
    jsBestSite1Str += '</div>'
    jsBestSite1Str += '</a>'
    if(response.data.beautyg.content.length-1==i){
      jsBestSite1Str += '<div class="clearFloat"></div>'
    }
  }
  $('.jsBestSite1').append(jsBestSite1Str)
  let jsBestSite2Str = ''
  for(let i=0;i<response.data.comic.content.length;i++){
    jsBestSite2Str += `<a href="${response.data.comic.content[i][device]}" target="_blank" onclick="trackEvent('首頁-发烧动漫小说', '${response.data.comic.content[i].title}-${i}', '${response.data.comic.content[i][device]}')">`
    jsBestSite2Str += '<a  href="'+response.data.comic.content[i][device]+'" target="_blank">'
    jsBestSite2Str += '<div class="siteWrap">'
    jsBestSite2Str += '<p>'+response.data.comic.content[i].title+'</p>'
    jsBestSite2Str += '</div>'
    jsBestSite2Str += '</a>'
    if(response.data.comic.content.length-1==i){
      jsBestSite2Str += '<div class="clearFloat"></div>'
    }
  }
  $('.jsBestSite2').append(jsBestSite2Str)

  //製作飄浮球
  var dt = new Date();
  var act = dt.getHours()*2%response.data.balloon.content.length
  /////
  var nowAPP = act
  getBImgClass(bImgHost + response.data.balloon.content[nowAPP].image, 'jsFixImg');
  $('.jsFixTitle').text(response.data.balloon.content[nowAPP].title);
  $('.jsFixUrl').attr('href',response.data.balloon.content[nowAPP][device])
  var min,sec
  min = Math.abs(dt.getMinutes()-30)
  sec = '0'
  //這是計時器
  var interval = setInterval(function() { 
    if(min>=0 && sec>=0){
      if (min == 0 && sec ==0){
        if(nowAPP<response.data.balloon.content.length-1){
          nowAPP = parseInt(nowAPP)+1
        }else{
          nowAPP = 0
        }
        getBImgClass(bImgHost + response.data.balloon.content[nowAPP].image, 'jsFixImg');
        $('.jsFixTitle').text(response.data.balloon.content[nowAPP].title);
        $('.jsFixUrl').attr('href',response.data.balloon.content[nowAPP][device])
        min = 30
        sec = 0
      } else{
        if(sec==0){
          min-=1
          sec = 59
        }else{
          sec -= 1
        }
      }
    }
    if(min<10){
      if(sec<10){
        $('.jsCountdown').text('0'+min+':0'+sec)
      }else{
        $('.jsCountdown').text('0'+min+':'+sec)
      }
    }else{
      if(sec<10){
        $('.jsCountdown').text(min+':0'+sec)
      }else{
        $('.jsCountdown').text(min+':'+sec)
      }
    }
}, 1000); 

  
})